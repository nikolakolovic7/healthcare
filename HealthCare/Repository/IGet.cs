/***********************************************************************
 * Module:  IGet.cs
 * Author:  Nikola
 * Purpose: Definition of the Interface Service.IGet
 ***********************************************************************/

using System;

namespace Repository
{
   public interface IGet<T,ID>
   {
      T GetFromID(ID id);
   
   }
}